package com.megatron.test.api;

import com.megatron.test.models.jsonModels.WeatherModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * здесь будут расположены все запросы
 */
public interface WeatherApi {

    // получаем погоду..(передать город и токен)
    @GET("weather") //?q={city}&appid={token}&units=metric&lang=ru
    Call<WeatherModel> getWeather(@Query("q") String city,
                                  @Query("appid") String token,
                                  @Query("units") String metric,
                                  @Query("lang") String lang
    );

}
