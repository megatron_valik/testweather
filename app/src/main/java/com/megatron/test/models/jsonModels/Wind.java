package com.megatron.test.models.jsonModels;

public class Wind {

    private int speed;
    private int deg;

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public Wind withSpeed(int speed) {
        this.speed = speed;
        return this;
    }

    public int getDeg() {
        return deg;
    }

    public void setDeg(int deg) {
        this.deg = deg;
    }

    public Wind withDeg(int deg) {
        this.deg = deg;
        return this;
    }

}
