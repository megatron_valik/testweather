package com.megatron.test.models.jsonModels;

public class Rain {

    private double _1h;

    public double get1h() {
        return _1h;
    }

    public void set1h(double _1h) {
        this._1h = _1h;
    }

    public Rain with1h(double _1h) {
        this._1h = _1h;
        return this;
    }

}
