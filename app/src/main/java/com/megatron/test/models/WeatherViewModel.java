package com.megatron.test.models;

import android.view.View;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.megatron.test.controller.WeatherController;
import com.megatron.test.models.jsonModels.WeatherModel;

public class WeatherViewModel extends ViewModel {
    private WeatherDataBindingModel dataBindingModel = new WeatherDataBindingModel();
    private MutableLiveData<WeatherDataBindingModel> viewData = new MutableLiveData<>();
    private WeatherController controller;
    private WeatherClickListener clickListener = new WeatherClickListener();

    public WeatherViewModel() {
        controller = new WeatherController(new WeatherController.WeatherDownloadListener() {
            @Override
            public void onStartDownload() {
                dataBindingModel.setDownloaded(true);
                viewData.setValue(dataBindingModel);
            }

            @Override
            public void onFinishDownload(WeatherModel weather) {
                dataBindingModel.setWeatherModel(weather);
                dataBindingModel.setDownloaded(false);
                dataBindingModel.setCity(weather.getName());
                dataBindingModel.setThrowableMessage(null);
                viewData.setValue(dataBindingModel);
            }

            @Override
            public void onFailureDownload(Throwable throwable) {
                dataBindingModel.setThrowableMessage(throwable.getMessage());
                dataBindingModel.setDownloaded(false);
                viewData.setValue(dataBindingModel);
            }
        });
    }

    /**
     * загрузка данных
     *
     * @param cityName   назва города
     * @param userUpdate пользователь обновляет
     */
    public void download(String cityName, boolean userUpdate) {
        if (controller != null &&
                // нет данных
                ((dataBindingModel.getWeatherModel() == null || dataBindingModel.getCity() == null)
                        || userUpdate) // если нажали кнопку обновить
        ) {
            controller.downloadWeather(cityName);
        }
    }

    /**
     * возвращаем данные
     *
     * @return данные
     */
    public LiveData<WeatherDataBindingModel> getViewData() {
        return viewData;
    }

    /**
     * нажатие кнопки обновить
     *
     * @return слушатель нажатия кнопки
     */
    public WeatherClickListener getClickListener() {
        return clickListener;
    }

    @Override
    protected void onCleared() {
        if (controller != null)
            controller.stop();
        super.onCleared();
    }

    // клас реализует метод нажатия на кнопку
    public class WeatherClickListener {
        public void onClick(View view) {
            download(dataBindingModel.getCity(), true);
        }
    }

}
