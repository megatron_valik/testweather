package com.megatron.test.models;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.megatron.test.BR;
import com.megatron.test.models.jsonModels.WeatherModel;

public class WeatherDataBindingModel extends BaseObservable {
    private String city;
    private WeatherModel weatherModel;
    private boolean isDownloaded;
    private String throwableMessage;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Bindable
    public WeatherModel getWeatherModel() {
        return weatherModel;
    }

    public void setWeatherModel(WeatherModel weatherModel) {
        this.weatherModel = weatherModel;
        notifyPropertyChanged(BR.weatherModel);
    }

    @Bindable
    public boolean isDownloaded() {
        return isDownloaded;
    }

    public void setDownloaded(boolean downloaded) {
        isDownloaded = downloaded;
        notifyPropertyChanged(BR.downloaded);
    }

    @Bindable
    public String getThrowableMessage() {
        return throwableMessage;
    }

    public void setThrowableMessage(String throwableMessage) {
        this.throwableMessage = throwableMessage;
        notifyPropertyChanged(BR.throwableMessage);
    }
}
