package com.megatron.test;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.megatron.test.databinding.ActivityMainBinding;
import com.megatron.test.models.WeatherDataBindingModel;
import com.megatron.test.models.WeatherViewModel;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        init();
    }

    ViewModelProvider.Factory factory = new ViewModelProvider.NewInstanceFactory() {
        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new WeatherViewModel();
        }
    };

    private void init() {
        WeatherViewModel viewModel = new ViewModelProvider(this, factory).get(WeatherViewModel.class);
        viewModel.getViewData().observe(this, bindingModel -> updateDataBinding(bindingModel, viewModel));
        viewModel.download("киев", false);
    }

    private void updateDataBinding(WeatherDataBindingModel bindingModel, WeatherViewModel viewModel) {
        if (binding != null) {
            binding.setWeather(bindingModel);
            binding.setWeatherClick(viewModel.getClickListener());
        }
    }

}