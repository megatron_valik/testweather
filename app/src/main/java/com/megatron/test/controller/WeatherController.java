package com.megatron.test.controller;

import androidx.annotation.NonNull;

import com.megatron.test.api.WeatherApi;
import com.megatron.test.api.WeatherBuilder;
import com.megatron.test.models.jsonModels.WeatherModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * контролер по работе с ретрофитом
 * получаем данные...
 */
public class WeatherController {
    public interface WeatherDownloadListener {
        void onStartDownload();

        void onFinishDownload(WeatherModel weather);

        void onFailureDownload(Throwable throwable);
    }

    private boolean isDownloading = false; // идет загрузка данных или нет
    private Call<WeatherModel> weatherRequest;
    private WeatherApi api; //апи получения данных
    private WeatherDownloadListener downloadListener; // слушатель загрузки

    /**
     * конструктор контроллера
     *
     * @param downloadListener слушатель загрузки
     */
    public WeatherController(@NonNull WeatherDownloadListener downloadListener) {
        this.downloadListener = downloadListener;
        api = new WeatherBuilder()
                .getRetrofit()
                .create(WeatherApi.class);
    }

    public void downloadWeather(String city) {
        if (isDownloading) return; // если ждем ответа - на выход

        isDownloading = true; // флаг загрузки
        // отобразить прогресс загрузки
        downloadListener.onStartDownload();

        // подготовка запроса данных
        weatherRequest = api.getWeather(city, WeatherBuilder.token, "metric", "ru");
        // получаем данные
        //noinspection NullableProblems
        weatherRequest.enqueue(new Callback<WeatherModel>() {
            @Override // есть данные
            public void onResponse(Call<WeatherModel> call, Response<WeatherModel> response) {
                isDownloading = false;
                WeatherModel weather = response.body();
                if (weather != null && weather.getCod() == 200) {
                    // спрятать прогресс и отобразить погоду
                    downloadListener.onFinishDownload(weather);
                } else {
                    downloadListener.onFailureDownload(new IllegalArgumentException("Fail json response"));
                }
            }

            @Override // ошибка получения данных
            public void onFailure(Call<WeatherModel> call, Throwable t) {
                isDownloading = false;// показать ошибку и спрятать прогресс
                downloadListener.onFailureDownload(t);
            }
        });
    }

    public void stop() {
        if (weatherRequest != null && !weatherRequest.isCanceled())
            weatherRequest.cancel();
    }
}
