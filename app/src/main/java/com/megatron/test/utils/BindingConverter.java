package com.megatron.test.utils;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;

import java.util.Locale;

public class BindingConverter {

    @BindingAdapter("android:text")
    public static void bindSportDate(@NonNull TextView textView, double value) {
        StringBuilder builder = new StringBuilder();
        builder.append(value);
        textView.setText(builder.toString());
    }
}
